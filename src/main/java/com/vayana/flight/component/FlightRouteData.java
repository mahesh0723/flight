package com.vayana.flight.component;

import com.vayana.flight.entity.FlightRouteEntity;
import com.vayana.flight.util.FlightUtil;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class FlightRouteData implements ApplicationRunner {
    private List<FlightRouteEntity> flightRouteEntityList = new ArrayList<>();

    @Override
    public void run(ApplicationArguments args) throws Exception {
        FlightUtil flightUtil = new FlightUtil();
        flightRouteEntityList.addAll(flightUtil.readAirlineRoutes());
    }

    public List<FlightRouteEntity> getFlightRouteEntityList() {
        return flightRouteEntityList;
    }
}
