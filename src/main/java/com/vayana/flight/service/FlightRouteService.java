package com.vayana.flight.service;

import com.vayana.flight.component.FlightRouteData;
import com.vayana.flight.entity.FlightRouteEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class FlightRouteService {
    @Autowired
    FlightRouteData flightRouteData;

    public Map<String, Integer> getTop3AirlineCoverMaxCity() {
        Set<String> airlineCitySet = new HashSet<>();
        Map<String, Integer> airlineCityCountMap = new HashMap<>();
        flightRouteData.getFlightRouteEntityList().forEach(flightRouteEntity -> {
            airlineCitySet.add(flightRouteEntity.getAirline().concat(":").concat(flightRouteEntity.getSourceAirport()));
            airlineCitySet.add(flightRouteEntity.getAirline().concat(":").concat(flightRouteEntity.getDestinationAirport()));
        });
        airlineCitySet.forEach(airlineCity -> {
            String[] str = airlineCity.split(":");
            airlineCityCountMap.putIfAbsent(str[0], 0);
            airlineCityCountMap.put(str[0], airlineCityCountMap.get(str[0]) + 1);
        });
        Map<String, Integer> returnMap = new LinkedHashMap<>();
        for (int i = 0; i < 3; i++) {
            Map.Entry<String, Integer> maxEntry = Collections.max(airlineCityCountMap.entrySet(), (Map.Entry<String, Integer> e1, Map.Entry<String, Integer> e2) -> e1.getValue()
                    .compareTo(e2.getValue()));
            returnMap.put(maxEntry.getKey(), maxEntry.getValue());
            airlineCityCountMap.remove(maxEntry.getKey(), maxEntry.getValue());
        }
        return returnMap;

    }

    public Map<String, Integer> findTop3AirlineWithDirectFlight() {
        List<FlightRouteEntity> flightRouteEntityList = flightRouteData.getFlightRouteEntityList();
        List<FlightRouteEntity> airlineWithDirectFlightList = new ArrayList<>();
        for (int i = 0; i < flightRouteEntityList.size(); i++) {
            if (flightRouteEntityList.get(i).getStops() == 0) {
                boolean airlineDirectReturnFlight = false;
                for (int j = i; j < flightRouteEntityList.size(); j++) {
                    if (flightRouteEntityList.get(i).getSourceAirport().equals(flightRouteEntityList.get(j).getDestinationAirport()) &&
                            flightRouteEntityList.get(i).getDestinationAirport().equals(flightRouteEntityList.get(j).getSourceAirport()) &&
                            flightRouteEntityList.get(i).getStops() == 0) {
                        airlineDirectReturnFlight = true;
                        flightRouteEntityList.remove(j);
                        break;
                    }
                }
                if (airlineDirectReturnFlight) {
                    airlineWithDirectFlightList.add(flightRouteEntityList.get(i));
                }
            }
        }
        
        Map<String, Integer> airlineWithDirectFlightCountMap = new HashMap<>();
        airlineWithDirectFlightList.forEach(airlineWithDirectFlight -> {
            airlineWithDirectFlightCountMap.putIfAbsent(airlineWithDirectFlight.getAirline(), 0);
            airlineWithDirectFlightCountMap.put(airlineWithDirectFlight.getAirline(), airlineWithDirectFlightCountMap.get(airlineWithDirectFlight.getAirline()) + 1);
        });

        Map<String, Integer> returnMap = new LinkedHashMap<>();
        for (int i = 0; i < 3; i++) {
            Map.Entry<String, Integer> maxEntry = Collections.max(airlineWithDirectFlightCountMap.entrySet(), (Map.Entry<String, Integer> e1, Map.Entry<String, Integer> e2) -> e1.getValue()
                    .compareTo(e2.getValue()));
            returnMap.put(maxEntry.getKey(), maxEntry.getValue());
            airlineWithDirectFlightCountMap.remove(maxEntry.getKey(), maxEntry.getValue());
        }
        return returnMap;
//
//        Map<String, Integer> airlineWithDirectFlight = new HashMap<>();
//        List<Map<String, Object>> airlineWithDirectFlightCount = flightRouteRepo.findTop3AirlineWithDirectFlight();
//
//        airlineWithDirectFlightCount.forEach(map -> {
//            String airlineWithDirectFlightKey = null;
//            Integer airlineWithDirectFlightValue = null;
//
//            for (Map.Entry<String, Object> entry : map.entrySet()) {
//
//                if (entry.getKey().equalsIgnoreCase("airline")) {
//                    airlineWithDirectFlightKey = (String) entry.getValue();
//                } else {
//                    airlineWithDirectFlightValue = ((BigDecimal) entry.getValue()).intValue();
//                }
//            }
//
//            airlineWithDirectFlight.put(airlineWithDirectFlightKey, airlineWithDirectFlightValue);
//        });
//
//        return MapUtil.sortByDescValue(airlineWithDirectFlight);
    }

    public Map<String, Integer> getTop10CityWithMaxAirline() {
        Set<String> airlineCitySet = new HashSet<>();
        Map<String, Integer> airlineCityCountMap = new HashMap<>();

        flightRouteData.getFlightRouteEntityList().forEach(flightRouteEntity -> {
            airlineCitySet.add(flightRouteEntity.getSourceAirport().concat(":").concat(flightRouteEntity.getAirline()));
            airlineCitySet.add(flightRouteEntity.getDestinationAirport().concat(":").concat(flightRouteEntity.getAirline()));
        });
        airlineCitySet.forEach(airlineCity -> {
            String[] str = airlineCity.split(":");
            airlineCityCountMap.putIfAbsent(str[0], 0);
            airlineCityCountMap.put(str[0], airlineCityCountMap.get(str[0]) + 1);
        });
        Map<String, Integer> returnMap = new LinkedHashMap<>();
        for (int i = 0; i < 10; i++) {
            Map.Entry<String, Integer> maxEntry = Collections.max(airlineCityCountMap.entrySet(), (Map.Entry<String, Integer> e1, Map.Entry<String, Integer> e2) -> e1.getValue()
                    .compareTo(e2.getValue()));
            returnMap.put(maxEntry.getKey(), maxEntry.getValue());
            airlineCityCountMap.remove(maxEntry.getKey(), maxEntry.getValue());
        }
        return returnMap;

//        Map<String, Integer> cityWithMaxAirline = new HashMap<>();
//
//        List<Map<String, Object>> airlineWithDirectFlightCount = flightRouteRepo.findTop10CityWithMaxAirline();
//        airlineWithDirectFlightCount.forEach(map -> {
//            String cityWithMaxAirlineKey = null;
//            Integer cityWithMaxAirlineValue = null;
//
//            for (Map.Entry<String, Object> entry : map.entrySet()) {
//
//                if (entry.getKey().equalsIgnoreCase("source_airport")) {
//                    cityWithMaxAirlineKey = (String) entry.getValue();
//                } else {
//                    cityWithMaxAirlineValue = ((BigInteger) entry.getValue()).intValue();
//                }
//            }
//
//            cityWithMaxAirline.put(cityWithMaxAirlineKey, cityWithMaxAirlineValue);
//        });
//
//        return MapUtil.sortByDescValue(cityWithMaxAirline);
    }

}
