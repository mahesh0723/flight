package com.vayana.flight.controller;

import com.vayana.flight.service.FlightRouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("flightroute")
public class FlightRouteController {
    @Autowired
    FlightRouteService flightRouteService;

    @RequestMapping(value = "airlines-with-max-city", method = RequestMethod.GET)
    public Map<String, Integer> getTop3AirlineCoverMaxCity() {
        return flightRouteService.getTop3AirlineCoverMaxCity();
    }

    @RequestMapping(value = "airlines-with-direct-flight", method = RequestMethod.GET)
    public Map<String, Integer> getTop3AirlineWithDirectFlight() {
        return flightRouteService.findTop3AirlineWithDirectFlight();
    }

    @RequestMapping(value = "city-with-max-airline", method = RequestMethod.GET)
    public Map<String, Integer> getTop10CityWithMaxAirline() {
        return flightRouteService.getTop10CityWithMaxAirline();
    }
}
