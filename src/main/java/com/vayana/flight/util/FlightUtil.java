package com.vayana.flight.util;

import com.vayana.flight.entity.FlightRouteEntity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class FlightUtil {
    public List<FlightRouteEntity> readAirlineRoutes() throws URISyntaxException, IOException {
        List<FlightRouteEntity> flightRouteEntityList = new ArrayList<>();
        ClassLoader classLoader = getClass().getClassLoader();

        try (InputStream inputStream = classLoader.getResourceAsStream("airline.dat");
             InputStreamReader streamReader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
             BufferedReader reader = new BufferedReader(streamReader)) {

            String line;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
                FlightRouteEntity flightRouteEntity = new FlightRouteEntity();
                String[] arrSplit = line.split(",");
                flightRouteEntity.setAirline(arrSplit[0].equalsIgnoreCase("\\N") ? null : arrSplit[0]);
                flightRouteEntity.setAirlineId(arrSplit[1].equalsIgnoreCase("\\N") ? null : arrSplit[1]);
                flightRouteEntity.setSourceAirport(arrSplit[2].equalsIgnoreCase("\\N") ? null : arrSplit[2]);
                flightRouteEntity.setSourceAirportId(arrSplit[3].equalsIgnoreCase("\\N") ? null : arrSplit[3]);
                flightRouteEntity.setDestinationAirport(arrSplit[4].equalsIgnoreCase("\\N") ? null : arrSplit[4]);
                flightRouteEntity.setDestinationAirportId(arrSplit[5].equalsIgnoreCase("\\N") ? null : arrSplit[5]);
                flightRouteEntity.setCodeshare(arrSplit[6].equalsIgnoreCase("\\N") ? null : arrSplit[6]);
                flightRouteEntity.setStops(arrSplit[7].equalsIgnoreCase("\\N") ? null : Integer.parseInt(arrSplit[7]));
                try {
                    flightRouteEntity.setEquipment(arrSplit[8].equalsIgnoreCase("\\N") ? null : arrSplit[8]);
                } catch (Exception e) {
                    flightRouteEntity.setEquipment(null);
                }
                flightRouteEntityList.add(flightRouteEntity);
            }

        }
        return flightRouteEntityList;
    }
}
