package com.vayana.flight;

import com.vayana.flight.controller.FlightRouteController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import java.util.HashMap;
import java.util.Map;

@SpringBootTest
class FlightApplicationTests {
    @Autowired
    private FlightRouteController flightRouteController;

    @Test
    void contextLoads() {
    }

    @Test
    void verifyTop3AirlineCoverMaxCity() {
        Map<String, Integer> expectedResult = new HashMap<>();
        expectedResult.put("AA", 434);
        expectedResult.put("UA", 432);
        expectedResult.put("AF", 387);
        Map<String, Integer> actualResult = flightRouteController.getTop3AirlineCoverMaxCity();
        Assert.isTrue(expectedResult.entrySet().stream()
                .allMatch(e -> e.getValue().equals(actualResult.get(e.getKey()))), "result didnt match");
    }

    @Test
    void verifyTop3AirlineWithDirectFlight() {
        Map<String, Integer> expectedResult = new HashMap<>();
        expectedResult.put("FR", 1242);
        expectedResult.put("AA", 1163);
        expectedResult.put("UA", 1065);
        Map<String, Integer> actualResult = flightRouteController.getTop3AirlineWithDirectFlight();
        Assert.isTrue(expectedResult.entrySet().stream()
                .allMatch(e -> e.getValue().equals(actualResult.get(e.getKey()))), "result didnt match");
    }

    @Test
    void verifyTop10CityWithMaxAirline() {
        Map<String, Integer> expectedResult = new HashMap<>();
        expectedResult.put("BKK", 98);
        expectedResult.put("FCO", 92);
        expectedResult.put("FRA", 100);
        expectedResult.put("HKG", 84);
        expectedResult.put("CDG", 109);
        expectedResult.put("LHR", 86);
        expectedResult.put("PEK", 81);
        expectedResult.put("AMS", 80);
        expectedResult.put("SIN", 83);
        expectedResult.put("DXB", 85);
        Map<String, Integer> actualResult = flightRouteController.getTop10CityWithMaxAirline();
        Assert.isTrue(expectedResult.entrySet().stream()
                .allMatch(e -> e.getValue().equals(actualResult.get(e.getKey()))), "result didnt match");
    }


}
